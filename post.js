/* eslint-disable no-console */
const fs = require('fs');
const { exec } = require('child_process');

const { NODE_ENV, sandBoxName, win32ExtensionPath, serverPath } = process.env;
const [extName] = process.platform === 'win32' ? __dirname.split('\\').reverse() : __dirname.split('/').reverse();

const target = NODE_ENV === 'development' ? sandBoxName : extName;
const from = process.platform === 'win32' ? `${__dirname}\\build\\` : `${__dirname}/build/`;
const to = process.platform === 'win32' ? `${win32ExtensionPath}\\${target}\\` : `${serverPath}/${target}/`;

fs.readdir(from, (err, files) => {
  files.forEach((file) => {
    const [ext] = file.split('.').reverse();
    if (NODE_ENV === 'development' && (ext === 'qext' || ext === 'wbl')) return;
    const fileNameOutput = ['qext', 'html'].includes(ext) || file === 'bundle.js'
      ? `${target}.${ext}`
      : file;
    exec(`${process.platform === 'win32' ? 'copy /y' : 'cp'} "${from}${file}" "${to}${fileNameOutput}"`, (errCopy) => {
      if (errCopy) {
        console.log(errCopy);
      }
      console.log(`${fileNameOutput} has been copied to ${target}`);
    });
  });
});
