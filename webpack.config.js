const webpack = require('webpack');
const WebpackShellPlugin = require('webpack-shell-plugin');
const ReplaceInFileWebpackPlugin = require('replace-in-file-webpack-plugin');

// set sandbox name and paths
process.env.sandBoxName = 'XYChart';
process.env.win32ExtensionPath = 'C:\\Users\\daria\\Documents\\Qlik\\Sense\\Extensions';
process.env.serverPath = '/Volumes/Qlik_Sense_Storage/StaticContent/Extensions';

// production or development mode
const { NODE_ENV } = process.env;

module.exports = {
    entry: "./src/index.js",
    output: {
        path: `${__dirname}/build/`,
        filename: 'bundle.js',
        libraryTarget: 'amd',
    },
    plugins: [
      new WebpackShellPlugin({
        // onBuildStart: ['npm run setMode'],
        onBuildExit: ['npm run _post']
      }),
      new ReplaceInFileWebpackPlugin([
        {
          dir: 'build',
          files: ['bundle.js'],
          rules: [
            {
              search: '!_Options__WEBPACK_IMPORTED_MODULE_11__["options"].commercialLicense',
              replace: '_Options__WEBPACK_IMPORTED_MODULE_11__["options"].commercialLicense',
            },
            {
              search: '!f.options.commercialLicense',
              replace: 'f.options.commercialLicense',
            },
          ],
        },
      ]),
      new webpack.DefinePlugin({
        'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV)
      }),
    ],
    optimization: {
      minimize: NODE_ENV == 'development' ? false : true,
    },
    watch: NODE_ENV == 'development',
    devtool: NODE_ENV == 'development' ? 'inline-source-map' : false,
    performance: { hints: false },
    mode: 'none',
    externals: [
      'qlik',
      'js/qlik',
    ],
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                exclude: [
                    /node_modules/
                ],
                loader: 'babel-loader',
                options: {
                  presets: ['@babel/react', '@babel/preset-env']
                }
            },
            {
                test: /\.css$/,
                use: ['style-loader', 'css-loader', {
                    loader: 'postcss-loader',
                    options: {
                        plugins: () => [require('autoprefixer')]
                    }
                }]
            }
        ]
    },
    resolve: {
      extensions: ['.js', '.jsx']
    }
}
