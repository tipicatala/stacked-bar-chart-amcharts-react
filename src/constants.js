import qlik from 'qlik';

const app = qlik.currApp(this);
const varApi = app.variable;

export const qlikObjects = {
  qlik,
  app,
  varApi,
};

export default qlikObjects;
