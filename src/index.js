export * from './initialProperties/initialProperties';
export * from './definition/definition';
export * from './paint';
export const resize = () => {};
// eslint-disable-next-line func-names
export const beforeDestroy = function () {
  window.qsStore[this.options.id] = null;
};
