import c from './store/constants';

export const idSetter = value => ({
  type: c.ADD_ID,
  value,
});

export const dataSetter = value => ({
  type: c.EDIT_DATA,
  value,
});

export const colorSetter = value => ({
  type: c.EDIT_DATA,
  value,
});