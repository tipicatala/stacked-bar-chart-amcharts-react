import c from './constants';

export const qId = (state = '', action) => {
  switch (action.type) {
    case c.ADD_ID:
      return action.value;
    default:
      return state;
  }
};

export const data = (state = '', action) => {
  switch (action.type) {
    case c.EDIT_DATA:
      return action.value;
    default:
      return state;
  }
};

