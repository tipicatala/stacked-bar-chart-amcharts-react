const constants = {
  ADD_ID: 'ADD_ID',
  EDIT_DATA: 'EDIT_DATA',
};

export default constants;
