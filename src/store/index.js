import {
    createStore,
    combineReducers,
    applyMiddleware
} from 'redux'
import { qId, data } from './reducers'
import stateData from './initialState'


const logger = store => next => action => {
  let result
  if (process.env.NODE_ENV === 'development') {
    console.groupCollapsed("dispatching", action.type)
    console.log('prev state', store.getState())
    console.log('action', action);
  }
  result = next(action)
  if (process.env.NODE_ENV === 'development') {
    console.log('next state', store.getState())
    console.groupEnd()
  }
}
const saver = store => next => action => {
  let result = next(action)
  const { qId } = store.getState();
  sessionStorage[qId] = JSON.stringify(store.getState())
  return result
}


const storeFactory = (initialState = stateData, id = 'foo') =>
applyMiddleware(logger, saver)(createStore)(
  combineReducers({ qId, data}),
  stateData);

export default storeFactory;
