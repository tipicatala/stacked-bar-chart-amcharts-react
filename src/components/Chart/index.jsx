import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import './chart.css';
import * as am4core from '@amcharts/amcharts4/core';
import * as am4charts from '@amcharts/amcharts4/charts';
import shortId from 'shortid';
import { dataSetter } from '../../actions';

export class ComponentName extends Component {
  componentDidMount() {
    this.renderChart();
  }

  componentDidUpdate() {
    this.renderChart();
  }

  componentWillUnmount() {
    if (this.chart) {
      this.chart.dispose();
    }
  }

  renderChart() {
    const { self } = this.props;
    const chart = am4core.create(this.chartId, am4charts.XYChart);
    chart.maskBullets = false;

    const { data } = this.props;
    chart.data = data;

    const categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
    categoryAxis.dataFields.category = 'team';
    categoryAxis.renderer.grid.template.location = 0;
    categoryAxis.renderer.grid.template.strokeWidth = 0;
    // categoryAxis.renderer.cellStartLocation = 0.2;
    // categoryAxis.renderer.cellEndLocation = 0.8;

    const valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
    valueAxis.min = 0;
    valueAxis.renderer.minGridDistance = 30;
    valueAxis.calculateTotals = true;
    valueAxis.renderer.grid.template.strokeOpacity = 0.2;
    valueAxis.renderer.grid.template.stroke = am4core.color('orange');
    valueAxis.renderer.grid.template.strokeWidth = 1;
    valueAxis.renderer.baseGrid.disabled = true;

    function createSeries(field, colorObj) {
      const series = chart.series.push(new am4charts.ColumnSeries());
      series.name = field;
      series.dataFields.valueY = field;
      series.dataFields.categoryX = 'team';
      series.sequencedInterpolation = true;

      series.stacked = true;

      const labelBullet = series.bullets.push(new am4charts.LabelBullet());
      labelBullet.label.text = '{valueY}';
      labelBullet.label.fill = am4core.color('#000');
      labelBullet.locationY = 0.5;

      const columnTemplate = series.columns.template;
      columnTemplate.tooltipText = '[bold]{name}[/]\n[font-size:14px]{categoryX}: {valueY}';
      columnTemplate.fillOpacity = 1;
      columnTemplate.strokeOpacity = 0;
      columnTemplate.fill = am4core.color(colorObj[field]);

      columnTemplate.events.on('hit', (event) => {
        const value = +event.target.dataItem.dataContext.qElemNumber; const dim = 0;
        self.backendApi.selectValues(dim, [value], true);
      });

      return series;
    }

    const colors = ['yellow', 'grey', 'orange'].reverse();

    const objColor = data.dimention2Values.reverse().reduce((acc, current, index) => {
      acc[current] = colors[index];
      return acc;
    }, {});

    data.dimention2Values.forEach(value => createSeries(value, objColor, chart, self));

    const totalSeries = chart.series.push(new am4charts.ColumnSeries());
    totalSeries.dataFields.valueY = 'total';
    totalSeries.dataFields.categoryX = 'team';
    totalSeries.stacked = true;
    totalSeries.hiddenInLegend = true;
    totalSeries.columns.template.strokeOpacity = 0;

    const totalBullet = totalSeries.bullets.push(new am4charts.LabelBullet());
    totalBullet.dy = -5;
    totalBullet.label.text = '{valueY.total}';
    totalBullet.label.hideOversized = false;
    totalBullet.label.background.fillOpacity = 0.2;
    totalBullet.label.fontWeight = 600;
    this.chart = chart;

    chart.legend = new am4charts.Legend();
    const markerTemplate = chart.legend.markers.template;
    markerTemplate.width = 6;
    markerTemplate.height = 6;
  }


  render() {
    this.chartId = shortId.generate();
    return (
      <div
        id={this.chartId}
        className="chart"
        onKeyUp={() => { }}
        role="button"
        tabIndex={0}
      />
    );
  }
}

function mapStateToProps(state) {
  return {
    data: state.data,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    dataSetterInStore: (value) => {
      dispatch(dataSetter(value));
    },
  };
}

ComponentName.propTypes = {
  data: PropTypes.array.isRequired,
  self: PropTypes.object.isRequired,
};

export default connect(mapStateToProps, mapDispatchToProps)(ComponentName);
