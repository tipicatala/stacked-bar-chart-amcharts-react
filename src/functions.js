/* eslint-disable no-plusplus */
export const getQsStoreLayout = () => ({
  isRendered: false,
  isStoreAdded: false,
});

export const buildD3Model = (layout) => {
  const { qMatrix } = layout.qHyperCube.qDataPages[0];


  function onlyUnique(value, index, self) {
    return self.indexOf(value) === index;
  }

  function rotate(matrix) {
    const result = [];
    for (let i = 0; i < matrix[0].length; i++) {
      const row = matrix.map(e => e[i]);
      result.push(row);
    }
    return result;
  }

  const r90 = rotate(qMatrix);
  const dimention2Values = r90[1].map(o => o.qText)
    .filter(onlyUnique);

  const data = [];
  let count = 0;

  qMatrix.forEach((d, i) => {
    data.forEach((o) => {
      if (qMatrix[i][0].qText === o.team) {
        count++;
      }
    });
    if (count === 0) {
      data.push({
        team: qMatrix[i][0].qText,
        total: 0,
        qElemNumber: qMatrix[i][0].qElemNumber,
      });
    } count = 0;
  });

  qMatrix.forEach((e, i) => {
    dimention2Values.forEach((el) => {
      if (qMatrix[i][1].qText === el) {
        const team = qMatrix[i][0].qText;
        const key = qMatrix[i][1].qText;
        data.forEach((o) => {
          if (o.team === team && qMatrix[i][2].qText !== 0) {
            o[key] = qMatrix[i][2].qText;
          }
        });
      }
    });
  });
  data.dimention2Values = dimention2Values;
  return data;
};

export default getQsStoreLayout;
