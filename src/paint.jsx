import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import storeFactory from './store';
import { getQsStoreLayout, buildD3Model, colors } from './functions';
import { idSetter, dataSetter } from './actions';
import Chart from './components/Chart/index';

export function paint($element, layout) {

  const self = this;

  const { qInfo } = layout;
  const { qId } = qInfo;

  // qsStore initialisation;
  if (!window.qsStore) window.qsStore = {};
  if (!window.qsStore[qId]) {
    window.qsStore[qId] = getQsStoreLayout();
  }

  // React store initialisation
  if (!window.qsStore[qId].isStoreAdded) {
    window.qsStore[qId].store = storeFactory(true, qId);
    window.qsStore[qId].store.dispatch(idSetter(qId));
    window.qsStore[qId].isStoreAdded = true;
  }

  const { store } = window.qsStore[qId];

  store.dispatch(dataSetter(buildD3Model(layout)));


  // rendering
  $element.attr('id', qId);

  if (!window.qsStore[qId].isRendered) {
    ReactDOM.render(
      <Provider store={store}>
        <Chart self={self} />
      </Provider>,
      document.getElementById(qId),
    );
    window.qsStore[qId].isRendered = true;
  }
}

export default paint;
