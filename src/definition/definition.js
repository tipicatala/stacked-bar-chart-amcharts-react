
export const definition = {
    type: "items",
            component: "accordion",
            items: {
                
                dimensions: {
                    uses: "dimensions",
                    min: 2,
                    max: 2
                },
                measures: {
                  uses: "measures",
                  min: 1,
                  max: 1
              },
              sorting: {
                  uses: "sorting"
              },

			  	settings : {
                    uses : "settings",
                    // items: {
					// 	MyColorPicker: {
					// 		label:"My color-picker",
					// 		component: "color-picker",
					// 		ref: "myColor",
					// 		type: "integer",
					// 		defaultValue: 3
					// 	}
					// }
				}
          }
};
